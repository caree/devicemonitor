
hosts = 
	[
		{id: 'local', name:'local', host:'127.0.0.1', subText: '本计算机'}
		, {id: 'ThinkPad', name:'ThinkPad', host:'192.168.48.52', subText: 'X200笔记本'}
		, {id: 'reader1', name:'智能货架读写器', host:'192.168.48.191', subText: '智能货架读写器'}
		, {id: 'google', name:'谷歌', host:'google.com', subText: 'google.com'}
	];

exports.hosts = function(){
	return hosts;
};


