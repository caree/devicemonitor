

var ping = require('ping');
var http = require('http');
var _ = require('underscore');



actPing();
setInterval(actPing, 3000);

exports.index = function(req, res){
	res.render('index', { title: 'Express' });
};
exports.test = function(req, res){
	res.render('test');
};
exports.grid = function(req, res){
	res.render('gridster');
};

function actPing(){
	hosts.forEach(function(host){
	    ping.sys.probe(host, function(result){
	    	if(result.isAlive){
		        var msg = 'host ' + host.name + ' is alive';
		        console.log(msg.info);
		        ep.emit('hostAlive', {id: host.id, host: host.host, name: host.name, status: 1, subText: host.subText});
	    	}else{
		        var msg = 'host ' + host.name + ' is dead';
		        console.log(msg.warn);
		        ep.emit('hostDead', {id: host.id, host: host.host, name: host.name, status: -1, subText: host.subText});
	    	}
	    });
	});	
}
